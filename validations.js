const validations = {}

validations.validateSingleRecord = (record)=>{
    if (record && "ts" in record && "val" in record){
        return true;
    }else{
        return false;
    }
}

validations.validateBatchFile = (file)=>{
    let parsedFile = null;
    try{
        parsedFile = JSON.parse(file);
    } catch (e){
        return {validationSuccess:false, file:parsedFile, error: e}
    }
    if (parsedFile.length){
        parsedFile = parsedFile.filter(record => validations.validateSingleRecord(record))
    }
    return {validationSuccess:true, file:parsedFile, error:null}

}

module.exports = validations;[]