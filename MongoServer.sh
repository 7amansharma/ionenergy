echo "Pulling latest mongo dockerimage"
docker pull mongo
echo "Pulling latest mongo dockerimage COMPLETE"
echo "=============="
echo "Stopping testMongo container if already running"
docker container stop $(docker container ls -q --filter name=testMongo)
echo "=============="
echo "Removing testMongo container if already exists"
docker rm $(docker ps -a -q --filter name=testMongo)
echo "=============="
echo "Running MongoDB server as testMongo container"
docker run -d -p 27017:27017 --name testMongo mongo