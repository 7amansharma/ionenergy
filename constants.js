constants = {}

constants.INPUT_DATE_FORMAT = "YYYY-MM-DD HH:mm:ss"

module.exports = constants;