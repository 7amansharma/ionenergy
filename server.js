const express = require("express");
const router = require("./routes");

const PORT = process.env.PORT || 9999

const server = express();
server.use(express.urlencoded({extended:false}));
server.use(express.json());

router(server).listen(PORT,()=>{
    console.log(`Server started on port: ${PORT}`)
});;
