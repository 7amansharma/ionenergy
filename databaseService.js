const { MongoClient } = require('mongodb');


let dbHostName = process.env.DB_HOST || "localhost";
let dbPort = process.env.DB_PORT || 27017;
let dbUser = process.env.DB_USER || null;
let dbPassword = process.env.DB_PASSWORD || null;

let dbUri = "mongodb://localhost:27017";

if (dbUser && dbPassword){
    dbUri = `mongodb://${dbUser}:${dbPassword}@${dbHostName}:${dbPort}`;
}else{
    dbUri = `mongodb://${dbHostName}:${dbPort}`;
}

let dbConn = null;

let databaseName = process.env.DATABASE_NAME || "dev"
let temperatureCollectionName = "temperature"

async function getDbConnection(){
    if (dbConn == null){
        try{
            let client  = new MongoClient(dbUri, { useUnifiedTopology: true });
            await client.connect();
            dbConn = client;
        }catch(e){
            console.log("Failure in databse connection");
            throw new Error(e);
        }
    }
    return dbConn;
}

async function getCollection(collectionName){
    let conn = await getDbConnection();
    return conn.db(databaseName).collection(collectionName);
}

const service = {};

service.initializeDb = async ()=>{
    try{
        let client  = new MongoClient(dbUri, { useUnifiedTopology: true });
        await client.connect();
        dbConn = client;
    }catch(e){
        console.log("Failure in databse connection");
        throw new Error(e);
    }
    return dbConn;
}; 


service.insertSingleRecord = async (collectionName,record)=>{
    let collection = await getCollection(collectionName);
    return await collection.insertOne(record);
}

service.insertMultipleRecords = async (collectionName,records)=>{
    let collection = await getCollection(collectionName)
    return await collection.insertMany(records);
}

service.fetchRecordsbyCondition = async (collectionName, condition)=>{
    let collection = await getCollection(collectionName)
    return await collection.find(condition);
}

service.fetchTemperatureByDateTime = async (dateFromEpoch=null, dateToEpoch=null)=>{
    let condition = { ts:{} }
    if (dateFromEpoch && dateToEpoch){
        condition.ts = {
            $gte:dateFromEpoch,
            $lt:dateToEpoch
        }
    }else  if (dateFromEpoch){
        condition.ts = {
            $gte:dateFromEpoch
        }
    }else if (dateToEpoch){
        condition.ts = {
            $lt:dateToEpoch
        }
    }
    return await service.fetchRecordsbyCondition(temperatureCollectionName, condition);
}



module.exports = service;