const multer = require("multer");
const upload = multer({storage:multer.memoryStorage()});

const apis = require("./apis");

module.exports = (app)=>{

    app.get('/', apis.homePage);
    app.get("/getVals",apis.getTemperatureValues);
    app.post("/batch",upload.single('file'),apis.handleBatchRecords);
    app.post("/realTime",apis.handleRealTimeRecords);

    return app

}