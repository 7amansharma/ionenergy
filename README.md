**run following commands to setup the environment**

cd path_to_root_of_project

npm install

Execute "MongoServer.sh" to run a mongo server on localhost:27017 using docker (docker must be installed on the machine)

**run the application server using following command**

PORT=<PORT_NUM> node server.js

(You can also pass DB_HOST (default: localhost), DB_PORT (default: 27017), DB_USER (default: null), DB_PASSWORD (default: null), DB_NAME (default: dev) as extra env variables )

**or**

node server.js ## defult port is 9999 in this case

**Go to the browser on open http://localhost:PORT_NUM/**

There will be three html forms

1. for batch records upload
2. single record upload
3. get records between a time range with required datetime format.

Correspondingly, expect following responses

1. for batch upload, number of successful record uploaded/inserted into the db (dublicate check is not implemented hence data will be duplicated if same data is sent again).
2. for single record, inserted status will be return if its a valid record otherwise an error message will be return.
3. will return a list of records matching the time range query.

Notes:

1. I have assumed, the real time records are received using http post.
2. Restarting the container will result in the loss of uploaded data in the database if MongoServer.sh is used to create the database (we can attach a storage location with the container to avoid it).
