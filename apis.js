const moment = require("moment");
const apis = {};
const validation = require("./validations");
const dbService = require("./databaseService");
const constants = require("./constants");

let temperatureCollectionName = "temperature";

apis.homePage = async (req, res) => {
    // HTML Form
    res.send(`<form action="/batch" method="POST" enctype="multipart/form-data">
    <label>Upload in batch</label><br>
    <input type="file" name="file" placeholder="Upload"><br>
    <input type="Submit" value="Send" >
</form>

<form action="/realTime" method="POST" enctype="application/json">
    <label>Send single record</label><br>
    <label>Time Stamp</label><br>
    <input type=number name="ts" ><br>
    <label>Value</label><br>
    <input type=number name="val" ><br>
    <input type="Submit" value="Send">
</form>


<form action="/getVals" method="GET">
    <label>Get Records</label><br>
    <label>Start Time (${constants.INPUT_DATE_FORMAT})</label><br>
    <input type=text name="dateFrom"  ><br>
    <label>End Time (${constants.INPUT_DATE_FORMAT})</label><br>
    <input type=text name="dateTo"  ><br>
    <input type="Submit" value="fetch">
</form>

`);
}

apis.handleBatchRecords = async (req, resp)=>{
    console.log(req.file);
    if (req.file){
        let {validationSuccess, file, error } = validation.validateBatchFile(req.file.buffer.toString('utf8'))
        let result;
        if (file){
            console.log(dbService);
            let response = await dbService.insertMultipleRecords(temperatureCollectionName, file);
            result = ({"success":true, noOfRecords:file.length, insertedCount:response.insertedCount});
        }else{
            result = "Please upload valid file";
        }
        resp.send(`
        ${JSON.stringify(result)}<br>
        <a href="/" ><button>Send Again</button></a> 
        `)


    }else{
        resp.send(`
        Please upload a file<br>
        <a href="/" ><button>Try Again</button></a> 
        `)
    }

}

apis.handleRealTimeRecords = async (req,resp)=>{
    console.log((req.body));
    let record = req.body;
    if (validation.validateSingleRecord(record)){
        let result = await dbService.insertSingleRecord(temperatureCollectionName, record)
        resp.send(`
        ${JSON.stringify(result)}<br>
        <a href="/" ><button>Send Again</button></a> 
        `)
    }else{
        resp.send(`
        Please send valid record<br>
        <a href="/" ><button>Try Again</button></a> 
        `)
    }

}

apis.getTemperatureValues = async (req, resp) => {
    let result;
    if (req.query){
        let dateFrom = req.query.dateFrom;
        let dateTo = req.query.dateTo;
        if (dateFrom || dateTo){
            try{
                if (dateFrom && dateTo){
                    dateFrom = moment(dateFrom).valueOf();
                    dateTo = moment(dateTo).valueOf();
                }else if (dateFrom){
                    dateFrom = moment(dateFrom).valueOf();
                    dateTo = null;
                }else if (dateTo){
                    dateFrom = null;
                    dateTo = moment(dateTo).valueOf();
                }
                console.log(dateFrom,dateTo)
                result =  await dbService.fetchTemperatureByDateTime(dateFrom, dateTo);
                result = await result.toArray();
            }catch(e){
                console.log(e);
                result = `Please send time in format ${constants.INPUT_DATE_FORMAT}`
            }

        }else{
            result = "Please send dateFrom and dateTo query parameter";
        }
    }else{
        result = "Please send dateFrom and dateTo query parameters";
    }
    console.log(req.query);

    console.log(result);
    resp.send(`
    ${JSON.stringify({"result":result})}<br>
    <a href="/" ><button>Send Again</button></a> 
    `)
}

module.exports = apis;